/**
 * Question.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    question: {
      type: 'string',
      required: true
    },
    details: {
      type: 'string',
      allowNull: true
    },
    note: {
      type: 'string',
      allowNull: true
    },
    answers: {
      type: 'string',
      allowNull: true
    },
    model: {
      type: 'string',
      allowNull: true
    },
    required: {
      type: 'boolean',
      defaultsTo: true
    },
    multiple: {
      type: 'boolean',
      defaultsTo: false
    },
    step: {
      type: 'number',
      required: true
    },
    active: {
      type: 'boolean',
      defaultsTo: true
    }
  }
};
